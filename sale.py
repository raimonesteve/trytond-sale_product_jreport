from trytond.pool import PoolMeta
from trytond.modules.jasper_reports.jasper import JasperReport
from trytond.model import fields

__all__ = ['Sale', 'SaleProductReport', 'SaleFranchiseReport',
    'SaleProductXlsReport', 'SaleFranchiseXlsReport']


class Sale:
    __metaclass__ = PoolMeta
    __name__ = 'sale.sale'
    total_packages = fields.Function(
        fields.Integer('Total packages'),
        'get_total_packages')

    def get_total_packages(self, name):
        total_packages = 0
        for line in self.lines:
            if line.number_of_packages:
                total_packages += line.number_of_packages
        return total_packages


class SaleProductReport(JasperReport):
    __name__ = 'sale.product.report'


class SaleProductXlsReport(JasperReport):
    __name__ = 'sale.product.xls.report'


class SaleFranchiseReport(JasperReport):
    __name__ = 'sale.franchise.report'


class SaleFranchiseXlsReport(JasperReport):
    __name__ = 'sale.franchise.xls.report'
