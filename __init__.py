from trytond.pool import Pool
from . import sale


def register():
    Pool.register(
        sale.Sale,
        module='sale_product_jreport', type_='model')
    Pool.register(
        sale.SaleProductReport,
        sale.SaleProductXlsReport,
        sale.SaleFranchiseReport,
        sale.SaleFranchiseXlsReport,
        module='sale_product_jreport', type_='report')
